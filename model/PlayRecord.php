<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18-1-16
 * Time: 下午2:21
 */

class PlayRecord
{
    // 主播UID => string
    private $hostUid = '';

    // 直播appid => int
    private $appid = 0;

    // 点赞数 => int
    private $admireCount = 0;

    // 当前观众数 => int
    private $watcherCount = 0;

    // 手术类型 => int;
    private $surgeryType;

    // 主刀医生 => string;
    private $surgeon;

    // 医院 => string;
    private $hospital;

    // 手术方案 => string;
    private $surgicalProgram;

    // 创建时间 => string
    private $createTime;

    // 修改时间 => string
    private $modifyTime;


    /* 功能：将直播记录存入数据库
     * 说明：成功返回插入的ID, 失败返回-1
     */
    public function save()
    {

    }

    /* 功能：删除直播记录
     * 说明：将用户hostUid的直播记录删除。一个用户同一时间只能开启一个直播；
     *       成功返回true 失败返回false
     */
    static public function delete($hostUid)
    {

    }

    /* 功能：删除死亡直播记录
     * 说明：超过inactiveSeconds时间间隔未收到主播心跳，则视为直播死亡，由定时清理程序调用删除
     *       成功返回true 失败返回false
     */
    public static function deleteInactiveRecord($inactiveSeconds)
    {

    }

    /* 功能：获取直播记录总数
     * 说明：超过inactiveSeconds时间间隔未收到主播心跳，则视为直播死亡，由定时清理程序调用删除
     *       成功返回直播总数，出错返回-1
     */
    public static function getCount($appid)
    {

    }

    /* 功能：查询直播记录列表
     * 说明：成功返回直播记录列表，失败返回null
     */
    public static function getLiveRoomList()
    {

    }

    /* 功能：查询直播记录
    * 说明：查询指定的记录字段
    */
    public static function getList($fields)
    {

    }

    /* 功能：筛选直播记录
    * 说明：根据直播信息筛选直播记录
    */
    public static function filterList($data)
    {

    }

    /* 功能：根据主播Uid更新直播数据
     * 说明：data   直播动态数据，目前主要是点赞数，和更新时间。成功：更新记录数;出错：-1
     */
    public static function updateByHostUid($hostUid, $data)
    {

    }

}