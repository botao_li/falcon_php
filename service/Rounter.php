<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18-1-16
 * Time: 上午9:23
 */

class Rounter
{
    private static $mapper = array(
        //device end
        'device' => array(
            'regist' => 'DeviceRegisterCmd',
            'login' => 'DeviceLoginCmd',
            'logout' => 'DeviceLogoutCmd',
            'createroom' => 'DeviceCreateRoomCmd',
            'exitroom' => 'DeviceExitRoomCmd',
            'reportroominfo' => 'DeviceReportRoomInfoCmd',
            'getwatcherinfo' => 'DeviceGetWatcherInfoCmd',
            'roommemberlist' => 'DeviceGetRoomMemberListCmd',
            'reportplayinfo' => 'DeviceReportPlayInfoCmd',
            'stopplay' => 'DeviceStopPlayCmd',
        ),

        'watcher' => array(
            //watcher end
            'register' => 'WatcherRegisterCmd',
            'login' => 'WatcherLoginCmd',
            'logout' => 'WatcherLogoutCmd',
            'changepwd' => 'WatcherChangePwdCmd',
            'authenticate' => 'WatcherAuthenticateCmd',
            'enterroom' => 'WatcherEnterRoomCmd',
            'roomlist' => 'WatcherGetRoomListCmd',
            'exitroom' => 'WatcherExitRoomCmd',
            'roommemberlist' => 'WatcherGetRoomMerberListCmd',
            'reportmenid' => 'WatcherReportMemidCmd',
            'getdeviceinfo' => 'WatcherGetDeivceInfoCmd',
            'filterroom' => 'WatcherFilterRoomCmd',
            'playlist' => 'WatcherFilterRoomCmd',
            'filterplay' => 'WatcherGetPlayListCmd',
        ),
        'monitor' => array(
            //monitor end
            'newaccount' => 'MonitorNewAccountCmd',
            'changepwd' => 'MonitorChangePwdCmd',
            'delaccount' => 'MonitorDelAccountCmd',
            'devicelist' => 'MonitorGetDeviceListCmd',
            'login' => 'MonitorLoginCmd',
            'logout' => 'MonitorLogoutCmd',
            'deldevice' => 'MonitorDelDeviceCmd',
            'modifydevice' => 'MonitorModifyDeviceCmd',
            'watcherlist' => 'MonitorGetWatcherListCmd',
            'delwatcher' => 'MonitorDelWatcherCmd',
            'modifywatcher' => 'MonitorModifyWatcherCmd',
        ),
    );

    public static function getCmdClassName($svc, $cmd)
    {
        if (!is_string($svc) || !is_string($cmd))
        {
            return '';
        }

        if (isset(self::$mapper[$svc]) && isset(self::$mapper[$svc][$cmd]))
        {
            return self::$mapper[$svc][$cmd];
        }
        return '';
    }
}